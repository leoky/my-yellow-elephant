@extends('layout.master') 

@section('containt')
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">One more step to the herds</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{route('addSubs')}}" method="POST">
      @csrf
      <div class="modal-body">
        <div class="form-group" >
          <input type="text"  style="border-radius:5px !important" name="name" class="form-control" placeholder= "Name">
        </div>
        <div class="form-group" >
          <input type="email"  style="border-radius:5px !important" name="email" class="form-control" placeholder= "E-mail">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button"  style="border-radius:5px !important" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit"  style="border-radius:5px !important" class="btn btn-primary">Subscribe</button>
      </div>
      </form>
    </div>
  </div>
</div>
  <div id="home">
  	<section class="ftco-cover" style="background-image: url(images/bg_100.jpg);" id="section-home" data-aos="fade"  data-stellar-background-ratio="0.5">
      <div class="container">
        <div class="row align-items-center ftco-vh-100">
          <div class="col-md-12">
            <h1 class="ftco-heading mb-3 main-color" data-aos="fade-up" data-aos-delay="500">We Care</h1>
            <h2 class="h5 ftco-subheading mb-5" data-aos="fade-up"  data-aos-delay="600">There are only an estimated 2,400 Sumatran elephants left in the wild, scattered accross 25 fragmented habitats on the island of Sumatra. <a href="https://myyellowelephant.com/" class="yellowtext" target="_blank">My Yellow Elephant</a> acts in saving the Sumatran Elephants.</h2>
            
             <button data-toggle="modal" data-target="#exampleModal" class="btn btn-warning whitetext" style="border-radius:25px; background-color: #ffbb00 !important;">Follow Our Story</button>
            
            
            <!-- <p data-aos="fade-up"  data-aos-delay="700"><a href="https://free-template.co/" target="_blank" class="btn btn-outline-white px-4 py-3" data-toggle="modal" data-target="#reservationModal">Go Get This Template</a></p> -->
          </div>
        </div>
      </div>
    </section>
    <!-- END section -->
    
    <section class="ftco-section">
      <div class="container">
        <div class="row">
          <div class="col-md-12  mb-5" data-aos="fade-up">
            <h1 class="ftco-heading heading-thin mb-5">Save The <span class="yellowtext">Sumatran Elephant</span>: They Belong To Where They Belongs.</h1>
          </div>
        </div>
        
      </div>
    </section>
    <!-- END section -->

    <section class="ftco-section-2">
      <div class="container-fluid">
        <div class="section-2-blocks-wrapper row no-gutters">
          <div class="img col-sm-12 col-md-5" style="background-image: url('images/image_4.jpg');" data-aos="fade-right">
            <a href="https://vimeo.com/45830194" class="button popup-vimeo" data-aos="fade-right" data-aos-delay="700"><span class="ion-ios-play"></span></a>
          </div>
          <div class="text col-md-6 col-lg-7 d-flex">
            <div class="text-inner align-self-center" data-aos="fade-up">
              
              <h3>What is <span class="yellowtext">My Yellow elephant</span>?</h3>
              <p>
              We are a team of university students from Medan that shares a goal of giving a better life for the Sumatran Elephants. We sell handicraft products from which a portion of the sales will be funded to various Sumatran Elephants conservation and NGO’s.</p>

             
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="section-2-blocks-wrapper-2">
      <div class="container-fluid">
        <div class="section-2-blocks-wrapper row no-gutters d-flex" style="background: #fafafa;">
          
          <div class="text col-md-5 col-lg-5 col-xl-4 ml-auto d-flex" data-aos="fade-up">
            <div class="text-inner align-self-center mr-auto">
              <!-- <h4 class="text-primary h5 yellowtext">My Yellow Elephant</h4>
              <p class="mb-4">Save Elephant Foundation is a Sumatera's non–profit organization dedicated to providing care and assistance to Sumatera’s captive elephant population through a multifaceted approach involving local community outreach, rescue and rehabilitation programs, and educational ecotourism operations. Each of our flagship projects is aimed at accomplishing that mission, as well as working towards our goals.</p> -->
              <h4 class="text-primary h5">Why Sumatera's Elephant</h4>
              <p class="mb-4">Sumatran elephants, also known as scientific names as Elephas maximus sumatranus, are subspecies of Asian elephants. Sumatran elephants have long roamed and inhabited the forests of the Sumatran islands. However, at present the condition of the existence of Sumatran elephants, which number 1,700 is very alarming and classified as critical status in the red list of endangered species by the World Conservation Institute (IUCN).</p>
              
              <p class="mb-4">Between 2012 and 2015, 36 elephants were found dead in Aceh Province due to poisoning, electrocution and traps. Most dead elephants were found near palm oil plantations. Conservationists think that Sumatran elephants may become extinct in less than ten years if poaching is not stopped.</p>
            </div>
          </div>
          <div class="img col-md-5 no-gutters ml-auto">
            <div class="image" style="background-image: url('images/image_5.jpg'); " data-aos="fade-left"></div>
            <div class="quote" data-aos="fade-left">
              <p class="mb-5 whitetext">&rdquo;
                The reason for the entry of Sumatran elephants into the red list is due to activities such as illegal logging and poaching. The Development of new plantations for palm oil industries has always been a large cause for deforestation hence causing habitat degradation. &ldquo;</p>
              <p class="author whitetext">&mdash; Charlotte</p>
            </div>
          </div>
        </div>
      </div>
      
    </section>
  </div>
    
  

  <div class="ftco-section" id="aboutus">
    <div class="container">
      <div class="row justify-content-center mb-5 pb-5">
        <!--<div class="col-md-7 text-center" data-aos="fade-up">-->
        <!--  <h2>About Us</h2>-->
        <!--</div>-->
      </div>
      <div class="row">
        <div class="col-md-6 col-lg-4"  data-aos="fade-up">
          <div class="flip-container hover">
            <div class="flipper">
              <div class="front" style="background-image: url(../images/image_5.jpg);">
                <div class="box">
                  <h2>Charlotte</h2>
                  <p>Founder</p>
                </div>
              </div>
              <div class="back">
                <!-- back content -->
                <blockquote>
                  <p>&ldquo;
                Increase awareness of the existence of elephants in Sumatra.&rdquo;</p>
                </blockquote>
                <div class="author d-flex">
                  <!-- <div class="image mr-3 align-self-center">
                    <img src="images/person_1.jpg" alt="">
                  </div> -->
                  <div class="name align-self-center">Charlotte <span class="position">Founder</span></div>
                </div>
              </div>
            </div>
          </div> <!-- .flip-container -->
        </div>
        <div class="col-md-6 col-lg-4"  data-aos="fade-up" data-aos-delay="100">
          
          <div class="flip-container hover">
            <div class="flipper">
              <div class="front" style="background-image: url(../images/image_6.jpg);">
                <div class="box">
                  <h2>Charlotte</h2>
                  <p>Founder</p>
                </div>
              </div>
              <div class="back">
                <!-- back content -->
                <blockquote>
                  <p>&ldquo;
                  How to be part of us is very easy, with every purchase of the products we sell, you can automatically become part of us who care about the life of a better Sumatran elephant.
                  &rdquo;</p>
                </blockquote>
                <div class="author d-flex">
                  <!-- <div class="image mr-3 align-self-center">
                    <img src="images/person_2.jpg" alt="">
                  </div> -->
                  <div class="name align-self-center">Marten <span class="position">Co Founder</span></div>
                </div>
              </div>
            </div>
          </div> <!-- .flip-container -->

        </div>
        <div class="col-md-6 col-lg-4"  data-aos="fade-up" data-aos-delay="200">
          
          <div class="flip-container hover">
            <div class="flipper">
              <div class="front" style="background-image: url(../images/image_7.jpg);">
                <div class="box">
                  <h2>Calvin FL</h2>
                  <p>Co Founder</p>
                </div>
              </div>
              <div class="back">
                <!-- back content -->
                <blockquote>
                  <p>&ldquo;Collaborating with organizations that protect elephants in Sumatra&rdquo;</p>
                </blockquote>
                <div class="author d-flex">
                  <!-- <div class="image mr-3 align-self-center">
                    <img src="images/person_3.jpg" alt="">
                  </div> -->
                  <div class="name align-self-center">Calvin FL <span class="position">Analyst</span></div>
                </div>
              </div>
            </div>
          </div> <!-- .flip-container -->

        </div>
      </div>
    </div>
  </div>

  <!-- <div class="ftco-section">
    <div class="container">
      <div class="row justify-content-center mb-5 pb-5">
        <div class="col-md-7 text-center" data-aos="fade-up">
          <h2>What We Do</h2>
        </div>
      </div>

      <div class="row">
        <div class="col-md-6 col-lg-4" data-aos="fade-up">
          <div class="media block-6">
            <div class="icon"><span class="flaticon-bank bg-after"></span></div>
            <div class="media-body">
              <h3 class="heading">Creative Performance</h3>
              <p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic.</p>
            </div>
          </div>      
        </div>
        <div class="col-md-6 col-lg-4" data-aos="fade-up" data-aos-delay="100">
          <div class="media block-6">
            <div class="icon"><span class="flaticon-career bg-after"></span></div>
            <div class="media-body">
              <h3 class="heading">Reach Performance</h3>
              <p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic.</p>
            </div>
          </div>      
        </div>
        <div class="col-md-6 col-lg-4" data-aos="fade-up" data-aos-delay="200">
          <div class="media block-6">
            <div class="icon"><span class="flaticon-lightbulb bg-after"></span></div>
            <div class="media-body">
              <h3 class="heading">Custom Development</h3>
              <p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic.</p>
            </div>
          </div>    
        </div>

        <div class="col-md-6 col-lg-4" data-aos="fade-up" data-aos-delay="300">
          <div class="media block-6">
            <div class="icon"><span class="flaticon-pie-chart bg-after"></span></div>
            <div class="media-body">
              <h3 class="heading">Creative Performance</h3>
              <p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic.</p>
            </div>
          </div>      
        </div>
        <div class="col-md-6 col-lg-4" data-aos="fade-up" data-aos-delay="400">
          <div class="media block-6">
            <div class="icon"><span class="flaticon-handshake bg-after"></span></div>
            <div class="media-body">
              <h3 class="heading">Reach Performance</h3>
              <p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic.</p>
            </div>
          </div>      
        </div>
        <div class="col-md-6 col-lg-4" data-aos="fade-up" data-aos-delay="500">
          <div class="media block-6">
            <div class="icon"><span class="flaticon-presentation bg-after"></span></div>
            <div class="media-body">
              <h3 class="heading">Custom Development</h3>
              <p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic.</p>
            </div>
          </div>    
        </div>

      </div>
    </div>
  </div> -->

  <!-- <div class="ftco-section bg-light">
    <div class="container">
      <div class="row justify-content-center mb-5 pb-5">
        <div class="col-md-7 text-center"  data-aos="fade-up">
          <h2>Our Blog</h2>
        </div>
      </div>

      <div class="row">
        <div class="col-md-6 col-lg-4" data-aos="fade-up">
          <a href="blog-single.html" class="block-5" style="background-image: url('images/image_10.jpg');">
            <div class="text">
              <div class="subheading">Travel</div>
              <h3 class="heading">Far far away, behind the word mountains</h3>
              <div class="post-meta">
                <span>Wellie Clark</span>
                <span>March 20, 2018</span>
              </div>
            </div>
          </a>
        </div>
        <div class="col-md-6 col-lg-4" data-aos="fade-up" data-aos-delay="100">
          <a href="blog-single.html" class="block-5" style="background-image: url('images/image_8.jpg');">
            <div class="text">
              <div class="subheading">Travel</div>
              <h3 class="heading">Far far away, behind the word mountains</h3>
              <div class="post-meta">
                <span>Wellie Clark</span>
                <span>March 20, 2018</span>
              </div>
            </div>
          </a>
        </div>
        <div class="col-md-6 col-lg-4" data-aos="fade-up" data-aos-delay="200">
          <a href="blog-single.html" class="block-5" style="background-image: url('images/image_9.jpg');">
            <div class="text">
              <div class="subheading">Travel</div>
              <h3 class="heading">Far far away, behind the word mountains</h3>
              <div class="post-meta">
                <span>Wellie Clark</span>
                <span>March 20, 2018</span>
              </div>
            </div>
          </a>
        </div>
      </div>

    </div>
  </div> -->

  
  <!-- <footer class="ftco-footer ftco-bg-dark ftco-section">
    <div class="container">
      <div class="row mb-5">
        <div class="col-md-8">
          <div class="row">
            <div class="col-md">
              <div class="ftco-footer-widget mb-4">
                <h2 class="ftco-heading-2">The Restaurant</h2>
                <ul class="list-unstyled">
                  <li><a href="#" class="py-2 d-block">About Us</a></li>
                  <li><a href="#" class="py-2 d-block">Chefs</a></li>
                  <li><a href="#" class="py-2 d-block">Events</a></li>
                  <li><a href="#" class="py-2 d-block">Contact</a></li>
                </ul>
              </div>
            </div>
            <div class="col-md">
               <div class="ftco-footer-widget mb-4">
                <h2 class="ftco-heading-2">Communities</h2>
                <ul class="list-unstyled">
                  <li><a href="#" class="py-2 d-block">Support</a></li>
                  <li><a href="#" class="py-2 d-block">Sharing is Caring</a></li>
                  <li><a href="#" class="py-2 d-block">Better Web</a></li>
                  <li><a href="#" class="py-2 d-block">Good Template</a></li>
                </ul>
              </div>
            </div>
            <div class="col-md">
               <div class="ftco-footer-widget mb-4">
                <h2 class="ftco-heading-2">Useful links</h2>
                <ul class="list-unstyled">
                  <li><a href="#" class="py-2 d-block">Bootstrap 4</a></li>
                  <li><a href="#" class="py-2 d-block">jQuery</a></li>
                  <li><a href="#" class="py-2 d-block">HTML5</a></li>
                  <li><a href="#" class="py-2 d-block">Sass</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="ftco-footer-widget mb-4">
            <ul class="ftco-footer-social list-unstyled float-md-right float-lft">
              <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
              <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
              <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 text-center">


        </div>
      </div>
    </div>
  </footer> -->

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>

@endsection