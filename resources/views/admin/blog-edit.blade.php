@extends('admin.layout.master') 

@section('containt')
<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<form action="{{route('editBlog',['id'=>$blog['id']])}}" method="post">
@csrf
<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <h1 class="h3 mb-0 text-gray-800">Edit Blog</h1>
  <div class="d-sm-inline-block">
    <button class="btn color-primary" type="submit">Save</button>
    <a href="/admin/blog"><div class="btn bg-secondary">Cancel</div></a>
  </div>
</div>
<p class="mb-4"></p>

    <div class="form-group">
        <label for="title">Title</label>
        <input type="text" name="title" class="form-control" id="title"  placeholder="Enter title" value="{{$blog['title']}}">
    </div>

    <div class="form-group">
        <label for="content">Content</label>
        <textarea class="form-control" id="content" rows="15" placeholder="Enter Content" name="body">{{$blog['body']}}</textarea>
    </div>
    
</div>
</form>


<!-- /.container-fluid -->
@endsection