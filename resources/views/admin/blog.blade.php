@extends('admin.layout.master') 

@section('containt')
<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <h1 class="h3 mb-0 text-gray-800">Blogs</h1>
  <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm" data-toggle="modal" data-target=".bd-example-modal-lg">Add Blog</a>
</div>
<p class="mb-4"></p>

<!-- DataTales  -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary">List Blog</h6>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>No</th>
            <th>Title</th>
            <th>Created at</th>
            <th>Action</th>
          </tr>
        </thead>
        <tfoot>
        
        </tfoot>
        <tbody>
            
          <tr>
            <!-- {{$no = 0}} -->
            @foreach($blogs as $blog)
            <tr>
                <td>{{++$no}}</td>
                <td>{{$blog['title']}}</td>
                <td>{{$blog['created_at']}}</td>
                <td>
                    <a href="{{route('getBlogById',['id'=>$blog['id']])}}"><button class="btn bg-secondary">Edit</button></a>
                    <form class="d-inline" action="{{route('deleteBlog',['id'=>$blog['id']])}}" method="post">
                        @csrf
                        <button class="btn bg-danger">Delete</button>
                    </form>
                </td>
            </tr>
            @endforeach
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>

</div>

<!-- modal create blog -->
<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <form action="{{route('addBlog')}}" method="post">
          @csrf
            <div class="modal-header">
                <h5 class="modal-title">New Blog</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" name="title" class="form-control" id="title"  placeholder="Enter title">
                </div>

                <div class="form-group">
                    <label for="content">Content</label>
                    <textarea class="form-control" id="content" rows="5" placeholder="Enter Content" name="body"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn color-primary">Save</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            </div>
        </form>
    </div>
    <!-- end modal content -->
  </div>
</div>

<!-- /.container-fluid -->
@endsection