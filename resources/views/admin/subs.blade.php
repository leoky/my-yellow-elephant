@extends('admin.layout.master') 

@section('containt')
<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-2 text-gray-800">Subscription</h1>
<p class="mb-4"></p>

<!-- DataTales Example -->
<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary">List Subscription</h6>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>No</th>
            <th>Name</th>
            <th>Email</th>
            <th>Created at</th>
            <th>Action</th>
          </tr>
        </thead>
        <tfoot>
        
        </tfoot>
        <tbody>
          <tr>
          <!-- {{$no = 0}} -->
          @foreach($subscribers as $sub)
            <tr>
                <td>{{++$no}}</td>
                <td>{{$sub['name']}}</td>
                <td>{{$sub['email']}}</td>
                <td>{{$sub['created_at']}}</td>
                <td>
                    <form class="d-inline" action="{{route('deleteSub',['id'=>$sub['id']])}}" method="post">
                        @csrf
                        <button type="submit" class="btn bg-danger" name="deleteid" value="{{$sub['id']}}">Delete</button>
                    </form> 
                </td>
            </tr>
            @endforeach
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>

</div>
<!-- /.container-fluid -->
@endsection