<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar" data-aos="fade-down" data-aos-delay="500">
      <div class="container">
        <a class="navbar-brand" href="#">
          <img src="{{asset('images/logo-width.png')}}" alt="" style="height: 50px; width:auto;">  
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="oi oi-menu"></span> Menu
        </button>

        <div class="collapse navbar-collapse" id="ftco-nav">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item"><a href="#home" class="nav-link" style="font-weight:800; font-size:18px;">Home</a></li>
            <li class="nav-item"><a href="#aboutus" class="nav-link" style="font-weight:800; font-size:18px;">About Us</a></li>
            <!--<li class="nav-item"><a href="#blogs" class="nav-link" style="font-weight:800; font-size:18px;">Blogs</a></li>-->
            <li class="nav-item"><a href="https://www.tokopedia.com/myyellowelephant" class="nav-link" target="_blank" style="margin-top:-3px;"><button class="btn btn-success my-2 my-sm-0" style="border-radius:50px;">Check Store!</button></a></li>
            <!-- <li class="nav-item"><a href="blog.html" class="nav-link">The Journal</a></li>
            <li class="nav-item"><a href="about.html" class="nav-link">Who We Are</a></li>
            <li class="nav-item"><a href="pricing.html" class="nav-link">Plans &amp; Pricing</a></li>
            <li class="nav-item"><a href="contact.html" class="nav-link">Contact</a></li> -->
          </ul>
        </div>
      </div>
    </nav>
    <!-- END nav -->