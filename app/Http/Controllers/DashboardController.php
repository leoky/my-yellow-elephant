<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subscribers;

class DashboardController extends Controller
{
    public function getHome(){
        return view('home');
    }

    public function addSubs(Request $request){
        $subs = new Subscribers;
        $subs->name = $request->input('name');
        $subs->email = $request->input('email');
        $subs->save();
        return redirect()->route('home');
    }
}
