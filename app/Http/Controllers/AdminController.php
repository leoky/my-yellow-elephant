<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Subscribers;
use App\Blogs;
use App\User;
use Auth;

class AdminController extends Controller
{
    public function __construct() 
    {
        // $this->middleware('auth')->except('getLogin','getRegister' );
    }

    public function getIndex(){
        $subs = Subscribers::all()->count();
        $blogs = Blogs::all()->count();
        return view('admin/home',[
            'subs'=>$subs,
            'blogs'=>$blogs
        ]);
    }

    public function getLogin(){
        return view('admin/login');
    }

    public function checkLogin(Request $request){
        if(Auth::attempt(['email'=>$request->email,'password'=>$request->password])){
            return redirect()->route('getAdminDashboard');            
        }else{
            return redirect()->route('getLogin');
        }
    }

    public function getRegister(){
        return view('admin/register');
    }

    public function postRegister(Request $request){
        $user = new User;
        $user->name = $request->input('first')." ".$request->input('last');
        $user->email = $request->input('email');
        $user->password = bcrypt($request->input('password'));
        $user->save();
        return redirect()->route('getLogin');
    }

    public function getSubs(){
        $subs = Subscribers::all();
        return view('admin/subs',[
            'subscribers'=>$subs
        ]);
    }

    public function deleteSub(Request $request, $id){
        $sub = Subscribers::find($id);
        $sub = $sub->delete();
        return redirect()->route('getSubs');
    }

    public function getBlogs(){
        $blogs = Blogs::all();
        return view('admin/blog',[
            'blogs' => $blogs
        ]);
    }

    public function addBlog(Request $request){
        $blog = new Blogs;
        $blog->title = $request->input('title');
        $blog->body = $request->input('body');
        $blog->created_by = 2;
        $blog->save();
        return redirect()->route('getBlogs');
    }

    public function getBlogById(Request $request, $id){
        $blog = Blogs::find($id);
        return view('admin/blog-edit',[
            'blog' => $blog
        ]);
    }

    public function editBlog(Request $request, $id){
        $blog = Blogs::find($id);
        $blog->title=$request->input('title');
        $blog->body=$request->input('body');
        $blog->save();
        return redirect()->route('getBlogs');
    }

    public function deleteBlog(Request $request, $id){
        
        $blog = Blogs::find($id);
        $blog = $blog->delete();
        return redirect()->route('getBlogs');
    }
}
