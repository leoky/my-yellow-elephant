<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'DashboardController@getHome')->name('home');
Route::post('/addSubs', 'DashboardController@addSubs')->name('addSubs');

Route::get('/admin/login', 'AdminController@getLogin')->name('getLogin');
Route::post('/admin/login', 'AdminController@checkLogin')->name('checkLogin');
Route::get('/admin/register', 'AdminController@getRegister')->name('getRegister');
Route::post('/admin/register', 'AdminController@postRegister')->name('postRegister');

Route::get('/admin', 'AdminController@getIndex',['middleware' => 'auth'])->name('getAdminDashboard');

Route::get('/admin/subscribe', 'AdminController@getSubs')->name('getSubs');
Route::post('/admin/subscribe/delete/{id}', 'AdminController@deleteSub')->name('deleteSub');

Route::get('/admin/blog', 'AdminController@getBlogs')->name('getBlogs');
Route::post('/admin/blog/addBlog','AdminController@addBlog')->name('addBlog');
Route::get('/admin/blog/{id}', 'AdminController@getBlogById')->name('getBlogById');
Route::post('/admin/blog/{id}', 'AdminController@editBlog')->name('editBlog');
Route::post('/admin/delete/blog/{id}', 'AdminController@deleteBlog')->name('deleteBlog');

Route::get('/logout', function(){
    Auth::logout();
    return redirect()->route('getLogin');
})->name('logout');






Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
